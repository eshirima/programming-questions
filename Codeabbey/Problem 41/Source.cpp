/********************************************************************
Author:		Emil Shirima
Date:       Saturday 04 April 2015

Purpose:	Solution to http://www.codeabbey.com/index/task_view/median-of-three

============= File Structure ================
Input data "Data.txt":
First Line: Contains the total number of lines in the file
The rest are integers in three

Output data "Result.txt":
Prints out the median of each three tuple

*********************************************************************/
#include<stdio.h>
#include<iostream>
#include<fstream>

int main(){
	std::ifstream source_file("Data.txt");
	std::ofstream destination_file("Result.txt");
	int count = 0, number_of_lines;
	int first = 0, second = 0, third = 0;
	int largest = 0, smallest = 0, median = 0;
	if (source_file.is_open()){
		// gets the total number of lines in the file
		source_file >> number_of_lines;
		while (count != number_of_lines){
			// reads the integers from the file
			source_file >> first >> second >> third;
			// performs the median check
			if (first > second){
				largest = first, smallest = second;
			}
			else{
				largest = second, smallest = first;
			}
			if (third > largest){
				median = largest;
			}
			else if (third < smallest){
				median = smallest;
			}
			else{
				median = third;
			}
			destination_file << median << "\n";
			++count;
		}
	}
	else{
		std::cerr << "Error opening file";
	}
	source_file.close();
	destination_file.close();
	return std::getchar();
}