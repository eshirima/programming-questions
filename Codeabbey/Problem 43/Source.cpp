/********************************************************************
Author:		Emil Shirima
Date:       Saturday 04 April 2015

Purpose:	Solution to http://www.codeabbey.com/index/task_view/dice-rolling

============= File Structure ================
Input data "Data.txt":
First Line: Contains the total number of lines in the file
The rest are doubles

Output data "Result.txt":
Prints out the respective dice roll number

*********************************************************************/
#include<stdio.h>
#include<iostream>
#include<fstream>
#include<math.h>

int main(){

	std::ifstream source_file("Data.txt");
	std::ofstream destination_file("Result.txt");

	int count = 0, number_of_lines;
	int dice_roll = 0;
	double number = 0.0;

	if (source_file.is_open()){
		// gets the total number of lines in the file
		source_file >> number_of_lines;
		while (count != number_of_lines){
			// reads the integers from the file
			source_file >> number;

			dice_roll = (number * 6) + 1;
			destination_file << dice_roll << "\n";

			++count;
		}
	}
	else{
		std::cerr << "Error opening file";
	}

	source_file.close();
	destination_file.close();

	return std::getchar();
}