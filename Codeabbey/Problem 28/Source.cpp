/********************************************************************
Author:		Emil Shirima
Date:       Saturday 04 April 2015

Purpose:	Solution to http://www.codeabbey.com/index/task_view/body-mass-index

============= File Structure ================
Input data "Data.txt":
First Line: Contains the total number of lines in the file
The rest are integers and doubles in pair

Output data "Result.txt":
Prints out the state of the person based on their calculated BMI

*********************************************************************/
#include<stdio.h>
#include<iostream>
#include<fstream>
#include<math.h>

double BMI_Calculator(int weight, double height){

	return weight / pow(height, 2);
}

int main(){

	std::ifstream source_file("Data.txt");
	std::ofstream destination_file("Result.txt");

	int count = 0, number_of_lines;
	int weight = 0;
	double height = 0.0;

	if (source_file.is_open()){
		// gets the total number of lines in the file
		source_file >> number_of_lines;
		while (count != number_of_lines){
			// reads the integers from the file
			source_file >> weight >> height;
			// performs the median check
			double answer = BMI_Calculator(weight, height);
			if (answer < 18.5){
				destination_file << "under" << "\n";
			}
			else if (answer >= 18.5 && answer < 25.0){
				destination_file << "normal" << "\n";
			}
			else if (answer >= 25.0 && answer < 30.0){
				destination_file << "over" << "\n";
			}
			else{
				destination_file << "obese" << "\n";
			}
			++count;
		}
	}
	else{
		std::cerr << "Error opening file";
	}

	source_file.close();
	destination_file.close();

	return std::getchar();
}