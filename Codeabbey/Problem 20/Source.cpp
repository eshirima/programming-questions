/********************************************************************
Author:		Emil Shirima
Date:       Saturday 04 April 2015

Purpose:	Solution to http://www.codeabbey.com/index/task_view/vowel-count

============= File Structure ================
Input data "Data.txt":
First Line: Contains the total number of lines in the file
The rest are strings of data

Output data "Result.txt":
Prints out the number of vowels in each line
PS:Ignore the first zero printed out.

*********************************************************************/
#include<stdio.h>
#include<iostream>
#include<fstream>
#include<string>

int main(){
	std::ifstream source_file("Data.txt");
	std::ofstream destination_file("Result.txt");
	int count = 0, number_of_lines;
	if (source_file.is_open()){
		// gets the total number of lines in the file
		source_file >> number_of_lines;
		while (count != number_of_lines){
			// gets an entire line in a file as string
			for (std::string line; std::getline(source_file, line);){
				int vowel_counter = 0;
				// checks the string for vowel contents
				for (int i=0; i < line.size(); ++i){
					if (line[i] == 'a' || line[i] == 'e' || line[i] == 'i' || line[i] == 'o' || line[i] == 'u' || line[i] == 'y'){
						++vowel_counter;
					}
				}
				destination_file << vowel_counter << "\n";
			}
			++count;
		}
	}
	else{
		std::cerr << "Error opening file";
	}
	source_file.close();
	destination_file.close();
	return std::getchar();
}